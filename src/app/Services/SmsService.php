<?php

namespace App\Services;

use App\Models\SMS\Sms;
use App\Repositories\Sms\SmsRepository;
use App\SMS\SmsGatewayProvider;

class SmsService
{
    /**
     * @param string $recipient Recipient.
     * @param string $message   Message.
     *
     * @return string
     */
    public static function sendSingleSms(string $recipient, string $message): Sms|array
    {
        $smsGatewayFactory = new SmsGatewayProvider();
        $smsGateway = $smsGatewayFactory->makeGateway();

        $result = $smsGateway->send($recipient, $message);

        /* @var    SmsRepository $repository */
        $repository = resolve(SmsRepository::class);

        $sms_item = $repository->store(
            $result + [
                Sms::RECIPIENT_NUMBER => $recipient,
                Sms::MESSAGE => $message,
                Sms::GATEWAY => class_basename($smsGateway)
            ]
        );

        return $sms_item;
    }
}
