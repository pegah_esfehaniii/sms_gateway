<?php

namespace App\Token;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;


class IssueToken
{
    /**
     * @param User $user User.
     */
    public function __construct(private readonly User $user)
    {
    }

    public function makeToken(): string|array
    {
        try {
            return $this->user->createToken('access token')->accessToken;
        } catch (\Exception $e) {
            Log::error('Generate Token Error: ' . $e);

            return [
                'error' => true,
                'message' => __('error.token_not_generated'),
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY
            ];
        }
    }
}
