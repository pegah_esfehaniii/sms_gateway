<?php

namespace App\Repositories\Sms;

use App\Models\SMS\Sms;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SmsRepository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Sms::class;
    }

    /**
     * @param array $data Data.
     *
     * @return Sms|array
     */
    public function store(array $data): array|Sms
    {
        try {
            $sms = Sms::query()->create([
                Sms::USER_ID => Auth::id(),
                Sms::MESSAGE => $data[Sms::MESSAGE],
                Sms::RECIPIENT_NUMBER => $data[Sms::RECIPIENT_NUMBER],
                Sms::STATUS => $data[Sms::STATUS],
                Sms::DELIVERY_CODE => $data[Sms::MESSAGE] ?? null,
                Sms::GATEWAY => $data[Sms::GATEWAY],
                Sms::GATEWAY_MESSAGE => $data[Sms::GATEWAY_MESSAGE] ?? null,

            ]);
        } catch (\Exception $e) {
            Log::error('Store SMS Error: ' . $e->getMessage());

            return [
                'error' => true,
                'message' => 'sms_not_store',
                'status' => Response::HTTP_BAD_REQUEST,
            ];
        }

        return $sms;
    }
}
