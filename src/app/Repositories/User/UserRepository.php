<?php

namespace App\Repositories\User;

use App\Interfaces\Models\User\UserInterface;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class UserRepository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return User::class;
    }

    /**
     * @param array $data Data.
     *
     * @return User|array
     */
    public function store(array $data): array|User
    {
        try {
            $password = bcrypt($data[User::PASSWORD]);
            $user = User::query()->create([
                User::NAME => $data[User::NAME],
                User::EMAIL => $data[User::EMAIL],
                User::PASSWORD => $password,

            ]);
        } catch (\Exception $e) {
            Log::error('Register Error: ' . $e->getMessage());

            return [
                'error' => true,
                'message' => 'user_not_registered',
                'status' => Response::HTTP_BAD_REQUEST,
            ];
        }

        return $user;
    }
}
