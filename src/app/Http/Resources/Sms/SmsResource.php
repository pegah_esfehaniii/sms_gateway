<?php

namespace App\Http\Resources\Sms;

use App\Http\Resources\User\UserResource;
use App\Models\SMS\Sms;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource.
 */
class SmsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            Sms::USER_ID => $this->{Sms::USER_ID},
            Sms::MESSAGE => $this->{Sms::MESSAGE},
            Sms::RECIPIENT_NUMBER => $this->{Sms::RECIPIENT_NUMBER},
            Sms::STATUS => $this->{Sms::STATUS},
            Sms::DELIVERY_CODE => $this->{Sms::DELIVERY_CODE},
            Sms::GATEWAY => $this->{Sms::GATEWAY},
            Sms::GATEWAY_MESSAGE => $this->{Sms::GATEWAY_MESSAGE},
            'user' => $this->whenLoaded(
                'user',
                function () {
                    return new UserResource($this->user);
                }
            ),
        ];
    }
}

