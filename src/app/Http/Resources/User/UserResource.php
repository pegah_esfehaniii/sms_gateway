<?php

namespace App\Http\Resources\User;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource.
 */
class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            User::ID => $this->{User::ID},
            User::NAME => $this->{User::NAME},
            User::EMAIL => $this->{User::EMAIL},
        ];
    }
}

