<?php

namespace App\Http\Requests\SMS;

use App\Models\SMS\Sms;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LoginRequest.
 */
class SmsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            Sms::MESSAGE => ['required', 'string'],
            Sms::RECIPIENT_NUMBER => ['required', 'string','max:10'],
        ];
    }
}
