<?php

namespace App\Http\Requests\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest.
 */
class RegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            User::NAME => ['required', 'string', 'max:50'],
            User::EMAIL => [
                'required',
                'email',
                Rule::unique(User::TABLE, User::EMAIL),
            ],
            User::PASSWORD => ['required', 'min:8'],

        ];

        return $rules;
    }

    /**
     * Set custom messages on validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'email.unique' => __('validation.user_email_must_be_unique'),
        ];
    }
}
