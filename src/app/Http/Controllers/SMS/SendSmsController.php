<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\Controller;
use App\Http\Requests\SMS\SmsRequest;
use App\Jobs\SendSmsJob;
use App\Models\SMS\Sms;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;


class SendSmsController extends Controller
{
    /**
     * Login user.
     *
     * @param SmsRequest $request SMS Request.
     *
     * @return JsonResponse Json Response.
     */
    public function __invoke(SmsRequest $request): JsonResponse
    {
        $data = $request->validated();

        dispatch(new SendSmsJob($data[Sms::RECIPIENT_NUMBER], $data[Sms::MESSAGE]));

        return $this->getResponse(['message' => 'sms sent'], ResponseAlias::HTTP_ACCEPTED);
    }
}
