<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Token\IssueToken;
use App\Utility\NumberSeries\Constants\NumberSeriesTypeConstant;
use App\Utility\NumberSeries\Generators\NumberSeriesBaseGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Login user.
     *
     * @param LoginRequest $request Login Request.
     *
     * @return JsonResponse Json Response.
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $identifierValue = $request->get(User::EMAIL);

        /** @var User $user */
        $user = User::query()->where(User::EMAIL, $identifierValue)
            ->orWhere(User::EMAIL, $identifierValue)->first();

        if (!$user) {
            return $this->getResponse(
                ['message' => __('error.user_is_not_active')],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!Hash::check($request->get(User::PASSWORD), $user->{User::PASSWORD})) {
            return $this->getResponse(
                ['message' => __('error.incorrect_user_pass')],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $generator = new IssueToken($user);
        $token = $generator->makeToken();

        if (isset($token['error'])) {
            return $this->getResponse(['message' => $token['message']], $token['status']);
        }

        return $this->getResponse([
            'token' => $token,
            'user' => new UserResource($user),
        ]);
    }


}
