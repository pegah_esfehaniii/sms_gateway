<?php

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Repositories\User\UserRepository;
use App\Token\IssueToken;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{

    /**
     * Register user.
     *
     * @param RegisterRequest $request    Register Request.
     * @param UserRepository  $repository Repository.
     *
     * @return JsonResponse Json Response.
     */
    public function register(RegisterRequest $request, UserRepository $repository): JsonResponse
    {
        $result = $repository->store($request->validated());

        if (isset($result['error'])) {
            return $this->getResponse(['message' => $result['message']], $result['status']);
        }

        $generator = new IssueToken($result);
        $token = $generator->makeToken();

        if (isset($token['error'])) {
            return $this->getResponse(['message' => $token['message']], $token['status']);
        }

        return $this->getResponse([
            'token' => $token,
            'user' => new UserResource($result),
        ]);
    }

}
