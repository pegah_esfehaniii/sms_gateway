<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param array|null $content    Content.
     * @param int        $statusCode Status Code.
     * @param array|null $heathers   Headers.
     *
     * @return JsonResponse
     */
    protected function getResponse(
        ?array $content = null,
        int $statusCode = Response::HTTP_OK,
        ?array $heathers = []
    ): JsonResponse {
        return response()->json(['data' => $content], $statusCode, $heathers);
    }
}
