<?php

namespace App\Models\SMS;


use App\Interfaces\Models\SMS\SmsInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Sms extends Model
{
    use HasFactory;

    const TABLE = 'smses';
    const ID = 'id';
    const USER_ID = 'user_id';
    const MESSAGE = 'message';
    const RECIPIENT_NUMBER = 'recipient_number';
    const STATUS = 'status';
    const DELIVERY_CODE = 'delivery_code';
    const GATEWAY = 'gateway';
    const GATEWAY_MESSAGE = 'gateway_message';
    const SENT = 'sent';
    const FAIL = 'fail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USER_ID,
        self::MESSAGE,
        self::RECIPIENT_NUMBER,
        self::STATUS,
        self::DELIVERY_CODE,
        self::GATEWAY,
        self::GATEWAY_MESSAGE
    ];

    /**
     * @var string
     */
    public $table = "smses";

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }


}
