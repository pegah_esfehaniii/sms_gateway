<?php

namespace App\SMS;

use App\Models\SMS\Sms;
use Illuminate\Support\Facades\Log;
use SoapClient;
use SoapFault;

class Rahyab implements SmsGateInterFace
{

    /** @var string */
    private string $username;

    /** @var string */
    private string $password;

    /** @var string */
    private string $senderNumber;

    /**
     * @var \SoapClient
     */
    private SoapClient $soapClientObj;

    /**
     * Create a new job instance.
     *
     * @return void
     * @throws \SoapFault SoapFault.
     */
    public function __construct()
    {
        $this->soapClientObj = new SoapClient(config('sms.gateways.rahyab')['server']);
        $this->username = config('sms.gateways.rahyab')['sender_number'];
        $this->password = config('sms.gateways.rahyab')['password'];
        $this->senderNumber = config('sms.gateways.rahyab')['sender_number'];
    }

    /**
     * @param string $recipient Recipient number.
     * @param string $message   Message text.
     *
     * @return array
     */
    public function send(string $recipient, string $message): array
    {
        try {
            $parameters = [
                'username' => $this->username,
                'password' => $this->password,
                'from' => $this->senderNumber,
                'to' => array($recipient),
                'text' => $message,
                'isflash' => false,
                'udh' => '',
                'recId' => array(0),
                'status' => array(0)
            ];
            $r = json_decode(json_encode($this->soapClientObj->SendSms($parameters)), true);

            return [
                Sms::STATUS => $r['SendSmsResult'] == 1 ? Sms::SENT : Sms::FAIL,
                Sms::DELIVERY_CODE => $r['recId']['long'] ?? null,
                Sms::GATEWAY_MESSAGE => $r['SendSmsResult']
            ];
        } catch (\SoapFault $fault) {
            $values = [
                Sms::STATUS => Sms::FAIL,
                Sms::GATEWAY_MESSAGE => $fault->getMessage()
            ];
            Log::error('Send Sms Error From Rahyab Gateway : ' . $fault->getMessage());
            return $values;
        }
    }

    /**
     * @return bool
     */
    public function canSend(): bool
    {
        if ($this->hasCredit() && $this->isNotExpired()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function hasCredit(): bool
    {
        try {
            return $this->soapClientObj->GetCredit(
                [
                        'username' => $this->username,
                        'password' => $this->password
                ]
            )->GetCreditResult > 314;
        } catch (SoapFault $fault) {
            Log::error('check credit  Error From Rahyab Gateway : ' . $fault->getMessage());
            return false;
        }
    }

    /**
     * @return bool
     */
    private function isNotExpired(): bool
    {
        $expireDate = $this->soapClientObj->GetExpireDate(
            [
                'username' => $this->username,
                'password' => $this->password
            ]
        )->GetExpireDateResult;
        $today = date('Y-m-d');

        return $expireDate > $today;
    }
}
