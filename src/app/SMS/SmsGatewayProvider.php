<?php

namespace App\SMS;

class SmsGatewayProvider
{
    /**
     * @return SmsGateInterFace
     */
    public function makeGateway(): SmsGateInterFace
    {
        $gatewayClasses = config('sms.map');
        $defaultGateway = config('sms.default');

        foreach ($gatewayClasses as $gatewayClass) {
            $gateway = new $gatewayClass();

            if ($gateway->canSend()) {
                return $gateway;
            }
        }

        return new ($gatewayClasses[$defaultGateway]);
    }
}
