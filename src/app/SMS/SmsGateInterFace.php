<?php

namespace App\SMS;

interface SmsGateInterFace
{

    /**
     * @param string $recipient Recipient number.
     * @param string $message    Message text.
     *
     * @return array
     */
    public function send(string $recipient, string $message): array;

    /**
     * @return bool
     */
    public function canSend(): bool;
}
