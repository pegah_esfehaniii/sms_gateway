<?php

namespace App\SMS;

use App\Models\SMS\Sms;
use Illuminate\Support\Facades\Log;

class Ghasedak implements SmsGateInterFace
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @param string $recipient Recipient number.
     * @param string $message   Message text.
     *
     * @return array
     */
    public function send(string $recipient, string $message): array
    {
        try {
            // this is mock for sending sms

            $values = [
                Sms::STATUS => Sms::SENT,
                Sms::DELIVERY_CODE => '12092831823',
                Sms::GATEWAY_MESSAGE => 'ghasedak message',
                Sms::GATEWAY => class_basename($this)
            ];
            return $values;
        } catch (\Exception $e) {
            $values = [
                Sms::STATUS => Sms::FAIL,
                Sms::GATEWAY_MESSAGE => $e->getMessage(),
                Sms::GATEWAY => class_basename($this)
            ];
            Log::error('Send Sms Error From Ghasedak Gateway : ' . $e->getMessage());
            return $values;
        }
    }

    /**
     * @return bool
     */
    public function canSend(): bool
    {
        // this is mock for checking sms gateway
        if ($this->hasCredit() && $this->isNotExpired()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function hasCredit(): bool
    {
        // this is mock for charge ability sms gateway
        return true;
    }

    /**
     * @return bool
     */
    private function isNotExpired(): bool
    {
        // this is mock for availability sms gateway
        return true;
    }

    //TODO :: we can check load balance or other parameters ...
}
