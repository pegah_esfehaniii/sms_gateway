<?php

use App\Models\SMS\Sms;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Sms::TABLE, function (Blueprint $table) {
            $table->increments(Sms::ID);
            $table->unsignedBigInteger(Sms::USER_ID)->nullable();
            $table->text(Sms::MESSAGE)->nullable();
            $table->string(Sms::RECIPIENT_NUMBER, 255)->nullable();
            $table->enum(Sms::STATUS, ['sent', 'fail', 'queued'])->default('queued')->nullable();
            $table->string(Sms::DELIVERY_CODE)->nullable();
            $table->string(Sms::GATEWAY)->nullable();
            $table->string(Sms::GATEWAY_MESSAGE)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Sms::TABLE);
    }

};

