<?php

namespace Database\Factories\SMS;

use App\Models\SMS\Sms;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class SmsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sms::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Sms::USER_ID => User::factory(),
            Sms::MESSAGE => $this->faker->word,
            Sms::RECIPIENT_NUMBER => $this->faker->numerify('##########'),
            Sms::STATUS => Sms::SENT,
            Sms::DELIVERY_CODE => $this->faker->word,
            Sms::GATEWAY => config('sms.default'),
            Sms::GATEWAY_MESSAGE => $this->faker->word
        ];
    }
}
