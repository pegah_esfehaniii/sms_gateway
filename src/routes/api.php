<?php

use App\Http\Controllers\SMS\SendSmsController;
use App\Http\Controllers\SMS\SmsController;
use App\Http\Controllers\User\LoginController;
use App\Http\Controllers\User\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', [RegisterController::class, 'register'])->name('register');
Route::post('login', [LoginController::class, 'login'])->name('login');

Route::middleware(['auth:api'])->group(
        function () {
            Route::post(
                'sms/send',
                SendSmsController::class
            )->name('sms.send');
            Route::apiResource('sms', SmsController::class)->only('index');
        }
    );
