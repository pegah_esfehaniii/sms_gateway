@extends('index-app')
@section('content')

    {{-- TODO Complete the blade ... using ajax ... --}}
    <div class="container-login100" style="background-image: url('/images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
            <form method="POST" action="{{ route('login') }}" role="form" data-parsley-validate
                  class="login-form validate-form">
                @csrf
                <span class="login100-form-title p-b-37">
					Sign In
				</span>

                <div class="wrap-input100 validate-input m-b-20" data-validate=" email">
                    <input class="input100" type="text" name="email" placeholder="username or email">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-25" data-validate="Enter password">
                    <input class="input100" type="password" name="password" placeholder="password">
                    <span class="focus-input100"></span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Sign In
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
