<!DOCTYPE html>
<html lang="fa-IR" dir="rtl">
<head>

    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">

    <meta name="developer" content="Pegah Esfehani">
    <meta name="author" content="Pegah Esfehani">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!------------------------------- theme css ------------------------------------->
    <link rel="stylesheet" href="{{ asset('/css/main.css')}}"/>
    <link rel="stylesheet" href="{{ asset('/css/util.css')}}"/>

    {{--    <!------------------------------Js Files -------------------------------------->--}}
    <script src="{{ asset('/js/main.js')}}"></script>

</head>
<body>

<div class="content">
    @yield('content')
</div>

</body>
</html>
