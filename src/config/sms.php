<?php

return [
    /*
    |----------------------------------------------------------------------
    | Default GateWay
    |----------------------------------------------------------------------
    |  This gateway is default but can be switched at runtime
    |
    */
    'default' => 'rahyab',

    /*
    |----------------------------------------------------------------------
    | List of GateWays
    |----------------------------------------------------------------------
    |
    */

    'gateways' => [
        'rahyab' => [
            'username ' => '500012188614156',
            'password' => '80818337',
            'sender_number' => '500012188614156',
            'server' => 'http://www.linepayamak.ir/Post/Send.asmx?wsdl'
        ],

        'ghasedak' => [
            'url' => 'http://api.iransmsservice.com',
            'apiKey' => 'Your api key',
            'from' => 'Your Default From Number',
        ],
    ],

    'map' => [
        'rahyab' => \App\SMS\Rahyab::class,
        'ghasedak' => \App\SMS\Ghasedak::class,
    ],
];
