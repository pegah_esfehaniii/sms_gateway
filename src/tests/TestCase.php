<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @param string $permission Permission.
     *
     * @return User
     */
    public function actingAsUser()
    {
        $loggedInUser = User::factory()->create();
        Passport::actingAs($loggedInUser);

        return $loggedInUser;
    }
}
