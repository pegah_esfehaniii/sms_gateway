<?php

namespace Tests\Feature;


use App\Models\SMS\Sms;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Http\Response;

class SendSmsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function userWithoutLoginCanNotSendSms()
    {
        $this->postJson(route('sms.send'), [])->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanNotSendSmsWithOutRequiredFields()
    {
        $this->actingAsUser();
        $response = $this->postJson(
            route('sms.send'), []
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $content = $response->getOriginalContent();

        $this->assertArrayHasKey(Sms::RECIPIENT_NUMBER, $content['errors']);
        $this->assertArrayHasKey(Sms::MESSAGE, $content['errors']);
    }

    /**
     * @test
     */
    public function userCanNotSendSmsWithIncorrectRecipientNumber()
    {
        $this->actingAsUser();

        $response = $this->postJson(
            route('sms.send'), [
                Sms::RECIPIENT_NUMBER => '912345675656',
                Sms::MESSAGE => 'test'
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $content = $response->getOriginalContent();

        $this->assertArrayNotHasKey(Sms::MESSAGE, $content['errors']);
        $this->assertArrayHasKey(Sms::RECIPIENT_NUMBER, $content['errors']);
    }

    /**
     * @test
     */
    public function userWithLoginCanSentSmsWithCorrectFields()
    {
        $this->actingAsUser();

        $response = $this->postJson(
            route('sms.send'), [
                Sms::RECIPIENT_NUMBER => '9191892439',
                Sms::MESSAGE => 'test'
            ]
        );
        $response->assertStatus(Response::HTTP_ACCEPTED);
    }

}
