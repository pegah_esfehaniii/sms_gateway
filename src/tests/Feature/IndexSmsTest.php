<?php

namespace Tests\Feature;


use App\Models\SMS\Sms;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Http\Response;

class IndexSmsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function userWithoutLoginCanNotSeeSmses()
    {
        $this->getJson(
            route('sms.index')
        )->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanSeeFloorList()
    {
        $this->actingAsUser();

        Sms::factory(3)->create();

        $response = $this->getJson(
            route('sms.index')
        );
        $response->assertOk();
        $response->assertJsonCount(3);
    }


}
