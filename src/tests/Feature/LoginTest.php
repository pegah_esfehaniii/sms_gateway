<?php

namespace Tests\Feature;

use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function testUserCanLoginWithCorrectDataEmail()
    {
        Artisan::call('passport:install', ['-vvv' => true]);
        $user = User::factory()->create();

        $response = $this->postJson(
            '/api/login',
            [
                User::EMAIL => $user->{User::EMAIL},
                User::PASSWORD => '123',
            ]
        );

        $response->assertOk();
        $this->assertIsArray($response->getOriginalContent());
        $this->assertArrayHasKey('token', $response->getOriginalContent()['data']);
        $this->assertArrayHasKey('user', $response->getOriginalContent()['data']);
        $this->assertInstanceOf(UserResource::class, $response->getOriginalContent()['data']['user']);
    }

    /**
     * @test
     */
    public function testUserCantLoginWithIncorrectPassword()
    {
        Artisan::call('passport:install', ['-vvv' => true]);
        $user = User::factory()->create();

        $response = $this->postJson(
            '/api/login',
            [
                User::EMAIL => $user->{User::EMAIL},
                User::PASSWORD => '123d',
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function testUserCantLoginWithIncorrectEmail()
    {
        Artisan::call('passport:install', ['-vvv' => true]);
        User::factory()->create();

        $response = $this->postJson(
            '/api/login',
            [
                User::EMAIL => 'a@a.com',
                User::PASSWORD => '123d',
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
