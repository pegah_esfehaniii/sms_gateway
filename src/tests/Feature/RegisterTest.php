<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function userWithNotExistingEmailCanRegisterAndLogin()
    {
        Artisan::call('passport:install', ['-vvv' => true]);
        $user = User::factory()->make();

        $response = $this->postJson(
            route('register'),
            [
                User::EMAIL => $user->{User::EMAIL},
                User::NAME => $user->{User::NAME},
                User::PASSWORD => $user->{User::PASSWORD}
            ]
        );

        $response->assertOk();
        $content = $response->getOriginalContent()['data'];
        $this->assertArrayHasKey('user', $content);
        $this->assertArrayHasKey('token', $content);
    }

    /**
     * @test
     */
    public function userCanNotRegisterWithIncorrectEmail()
    {
        Artisan::call('passport:install', ['-vvv' => true]);

        $response = $this->postJson(
            route('register'),
            [
                User::NAME => 'name',
                User::EMAIL => 'no_email',
                User::PASSWORD => '12345678',
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $content = $response->getOriginalContent();
        $this->assertArrayHasKey(User::EMAIL,  $content['errors']);
        $this->assertArrayNotHasKey(User::PASSWORD,  $content['errors']);
        $this->assertArrayNotHasKey(User::NAME,  $content['errors']);
    }

    /**
     * @test
     */
    public function userCanNotRegisterWithIncorrectPassword()
    {
        Artisan::call('passport:install', ['-vvv' => true]);
        $user = User::factory()->make();

        $response = $this->postJson(
            route('register'),
            [
                User::EMAIL => $user->{User::EMAIL},
                User::NAME => $user->{User::NAME},
                User::PASSWORD => '123'
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $content = $response->getOriginalContent();

        $this->assertArrayNotHasKey(User::EMAIL, $content['errors']);
        $this->assertArrayHasKey(User::PASSWORD, $content['errors']);
    }


}
